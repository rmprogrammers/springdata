-- users

CREATE TABLE public.users
(
    username text NOT NULL,
    password text NOT NULL,
    enabled boolean,
    PRIMARY KEY (username)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to admin;

-- authorities

CREATE TABLE public.authorities
(
    username text COLLATE pg_catalog."default" NOT NULL,
    authority text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT authority_key UNIQUE (username, authority),
    CONSTRAINT authorities_users FOREIGN KEY (username)
        REFERENCES public.users (username) MATCH SIMPLE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.authorities
    OWNER to admin;

CREATE INDEX fki_authorities_users
    ON public.authorities USING btree
    (username COLLATE pg_catalog."default")
    TABLESPACE pg_default;


-- SkillCategory

CREATE TABLE public.SkillCategory
(
    id SERIAL PRIMARY KEY,
    name text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.SkillCategory
    OWNER to admin;

-- Skill

CREATE TABLE public.Skill
(
    id SERIAL PRIMARY KEY,
    name text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    categoryId integer,
    CONSTRAINT skill_category FOREIGN KEY (categoryId)
        REFERENCES public.SkillCategory (id) MATCH SIMPLE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.Skill
    OWNER to admin;

CREATE INDEX fki_skill_category
    ON public.Skill USING btree
    (categoryId)
    TABLESPACE pg_default;

-- Progress

CREATE TABLE public.Progress
(
  username text NOT NULL,
  skillId integer NOT NULL,
  level smallint NOT NULL DEFAULT 0,
  progress integer DEFAULT NULL,
  CONSTRAINT Progress_pk PRIMARY KEY (username,skillId),
    CONSTRAINT progress_skill FOREIGN KEY (skillid)
        REFERENCES public.Skill (id) MATCH SIMPLE,
    CONSTRAINT progress_user FOREIGN KEY (username)
        REFERENCES public.users (username) MATCH SIMPLE
) 
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE INDEX fki_progress_skill
    ON public.Progress USING btree
    (skillid)
    TABLESPACE pg_default;

CREATE INDEX fki_progress_user
    ON public.Progress USING btree
    (username COLLATE pg_catalog."default")
    TABLESPACE pg_default;

