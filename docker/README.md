# Docker Directions

## To launch postgres in docker the first time:

```
yourrepo/docker> docker-compose up --build
```

It will create a new database, run all the initialization scripts against it, and leave it up for you to interact with 
on localhost:5432.

## To stop your machine: 
`Ctrl-C`
 
## To restart the same machine:

```
yourrepo/docker> docker-compose up
```

Using the build flag for this will not affect anything, it just isn't necessary. Changes to the initialization scripts
will be ignored after the first run, so you'd need to rebuild the image to execute them.

## To blow away your machine:

```
yourrepo/docker> docker-compose rm
```

This will let you rebuild your db from scratch. All your data will be lost, but you can start over at 
the top to create a new image with your most recent initialization scripts.