package org.rmpg.mastery.ui;

import org.rmpg.mastery.domain.SkillReport;
import org.rmpg.mastery.repository.ApprenticeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SkillReportController
{
    @Autowired
    private ApprenticeDao dao;

    @RequestMapping("/report")
    public SkillReport apprentice()
    {
        // ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        // return attr.getRequest().getSession(true); // true == allow create

        return dao.getSkillReport("user");
    }
}
