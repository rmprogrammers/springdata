package org.rmpg.mastery.rest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/programmer/{programmerId}/logbook")
public class Logbook
{
    @PostMapping(value = "/entry")
    public String addEntry(@PathVariable int programmerId)
    {
        return Integer.toString(programmerId);
    }
}
