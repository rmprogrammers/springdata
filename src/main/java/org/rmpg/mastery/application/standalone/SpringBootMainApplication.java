package org.rmpg.mastery.application.standalone;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = { "org.rmpg.mastery.application.config", "org.rmpg.mastery.rest",
    "org.rmpg.mastery.ui", "org.rmpg.mastery.repository" })
@EntityScan("org.rmpg.mastery.domain")
public class SpringBootMainApplication
{
    public static void main(String[] args) // NOPMD - use the cannonical signature
    {
        SpringApplication.run(SpringBootMainApplication.class, args);
    }

    @Bean
    public DataSource masteryDb()
    {
        org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl("jdbc:postgresql://localhost:5500/mastery");
        ds.setUsername("admin");
        ds.setPassword("password");
        return ds;
    }
}
