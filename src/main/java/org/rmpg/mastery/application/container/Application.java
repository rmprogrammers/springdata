package org.rmpg.mastery.application.container;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan(basePackages = { "org.rmpg.mastery.application.config", "org.rmpg.mastery.rest", "org.rmpg.mastery.ui",
    "org.rmpg.mastery.repository", "org.rmpg.mastery.domain" })
@EnableAutoConfiguration
@ImportResource("classpath:db.xml")
public class Application
    extends SpringBootServletInitializer
{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.sources(Application.class);
    }
}
