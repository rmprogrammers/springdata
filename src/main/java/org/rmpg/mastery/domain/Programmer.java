package org.rmpg.mastery.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Programmer
{
    @Id
    public String username;

    public String password;

    public boolean enabled;
}
