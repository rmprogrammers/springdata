package org.rmpg.mastery.domain;

public class Skill
{
    private String name;
    private SkillLevel level;
    private int progress;

    public Skill(String name, SkillLevel level, int progress)
    {
        this.name = name;
        this.level = level;
        this.progress = progress;
    }

    public String getName()
    {
        return name;
    }

    public SkillLevel getLevel()
    {
        return level;
    }

    public int getProgress()
    {
        return progress;
    }
}
