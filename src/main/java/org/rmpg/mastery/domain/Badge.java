package org.rmpg.mastery.domain;

public class Badge
{
    private String name;
    private SkillLevel level;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public SkillLevel getLevel()
    {
        return level;
    }

    public void setLevel(SkillLevel level)
    {
        this.level = level;
    }
}
