package org.rmpg.mastery.domain;

public class Apprentice
{
    private int id;
    private String name;

    public Apprentice(int id, String name)
    {
        super();
        this.id = id;
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

}
