package org.rmpg.mastery.domain;

public enum SkillLevel
{
    NOVICE, COMPETENT, PROFICIENT, EXPERT, MASTER;
}
