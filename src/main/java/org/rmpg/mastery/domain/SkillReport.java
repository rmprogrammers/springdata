package org.rmpg.mastery.domain;

import java.util.ArrayList;
import java.util.List;

public class SkillReport
{
    private String name;
    private List<SkillCategory> categories = new ArrayList<>();

    public SkillReport(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void addCategory(SkillCategory category)
    {
        categories.add(category);
    }

    public List<SkillCategory> getCategories()
    {
        return categories;
    }
}
