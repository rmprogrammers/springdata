package org.rmpg.mastery.domain;

import java.util.ArrayList;
import java.util.List;

public class SkillCategory
{
    private String name;
    private List<Skill> skills = new ArrayList<>();

    public SkillCategory(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public List<Skill> getSkills()
    {
        return skills;
    }

    public void addSkill(Skill skill)
    {
        skills.add(skill);
    }
}
