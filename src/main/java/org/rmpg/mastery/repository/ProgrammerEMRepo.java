package org.rmpg.mastery.repository;

import javax.persistence.EntityManager;

import org.rmpg.mastery.domain.Programmer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProgrammerEMRepo
{
    @Autowired
    private EntityManager em;

    public Programmer getUser()
    {
        return em.find(Programmer.class, "user");
    }
}
