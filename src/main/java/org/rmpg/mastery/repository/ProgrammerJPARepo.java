package org.rmpg.mastery.repository;

import org.rmpg.mastery.domain.Programmer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProgrammerJPARepo
    extends JpaRepository<Programmer, String>
{
}