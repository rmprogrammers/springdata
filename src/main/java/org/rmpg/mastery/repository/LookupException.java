package org.rmpg.mastery.repository;

public class LookupException
    extends Exception
{
    private static final long serialVersionUID = 1028397150606913593L;

    public LookupException(String message)
    {
        super(message);
    }

    public LookupException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
