package org.rmpg.mastery.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.rmpg.mastery.domain.Skill;
import org.rmpg.mastery.domain.SkillCategory;
import org.rmpg.mastery.domain.SkillLevel;
import org.rmpg.mastery.domain.SkillReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

@Repository
public class ApprenticeDao
{
    private JdbcTemplate template;

    private static class ReportExtractor
        implements ResultSetExtractor<SkillReport>
    {
        private SkillReport report;
        private SkillCategory category = null;

        public ReportExtractor(String name)
        {
            this.report = new SkillReport(name);
        }

        @Override
        public SkillReport extractData(ResultSet rs)
            throws SQLException, DataAccessException
        {
            category = null;
            while (rs.next())
            {
                updateCategory(rs);
                createSkill(rs);
            }

            return report;
        }

        private void updateCategory(ResultSet rs)
            throws SQLException
        {
            if (category == null || !category.getName().equals(rs.getString(1)))
            {
                category = new SkillCategory(rs.getString(1));
                report.addCategory(category);
            }
        }

        private void createSkill(ResultSet rs)
            throws SQLException
        {
            SkillLevel level = SkillLevel.values()[rs.getInt(3)];
            Skill skill = new Skill(rs.getString(2), level, rs.getInt(4));
            category.addSkill(skill);
        }
    }

    @Autowired
    public void setDataSource(@Qualifier("masteryDb") DataSource dataSource)
    {
        template = new JdbcTemplate(dataSource);
    }

    public SkillReport getSkillReport(String name)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT c.name AS categoryname, s.name AS skillname, p.level, p.progress ");
        sql.append("  FROM Progress p ");
        sql.append("       JOIN Skill s ON p.skillId = s.id ");
        sql.append("       JOIN SkillCategory c ON c.id = s.categoryId ");
        sql.append(" WHERE username = ? ");
        sql.append(" ORDER BY categoryname, skillname");

        return template.query(sql.toString(), new Object[] { name }, new ReportExtractor(name));
    }
}
