CREATE TABLE users
(
   username   varchar(50) NOT NULL PRIMARY KEY,
   password   varchar(255) NOT NULL,
   enabled    boolean NOT NULL
)
ENGINE = InnoDb;

CREATE TABLE authorities
(
   username    varchar(50) NOT NULL,
   authority   varchar(50) NOT NULL,
   FOREIGN KEY(username) REFERENCES users(username),
   UNIQUE INDEX authorities_idx_1(username, authority)
)
ENGINE = InnoDb;

CREATE TABLE `SkillCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `desc` text,
  `categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `SKILLCATEGORY_FK` (`categoryId`),
  CONSTRAINT `SKILLCATEGORY_FK` FOREIGN KEY (`categoryId`) REFERENCES `SkillCategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Progress` (
  `username` varchar(50) NOT NULL,
  `skillId` int(11) NOT NULL,
  `progress` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`,`skillId`),
  KEY `PROGRESSSKILL_FK` (`skillId`),
  CONSTRAINT `PROGRESSUSER_FK` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `PROGRESSSKILL_FK` FOREIGN KEY (`skillId`) REFERENCES `Skill` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
