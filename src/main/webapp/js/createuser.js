createUser = function() {
	var form;
	var initialized = false;
	
	return {
		init : function(formSection) {
			if(initialized) return;
			
			form = formSection;
			initalized = true;
			
			$(form).find('.submit').click(function(evt) {
				evt.preventDefault();
				
				if($(form).valid())
					return;
			})
		}
	};
}();