skillReportController = function () {
	var reportTable;
	var initialized = false;
	
	return {
		init : function(table) {
			if(initialized) return;
			
			reportTable = table;
			$.get('/report').done( 
					function(reportModel){ 
						writeReport(reportModel,reportTable)
						}
					);
			initialized = true;
		}
	}
}();

function writeReport(reportModel,reportTable) {
	var reportView = [ ];
	
	reportView.push(buildProgrammerName(reportModel));
	$.merge(reportView,buildCategories(reportModel.categories));
	
	for(var element of reportView) {
		$(reportTable).append(element);
	}
}

function buildCategories(categoriesModel) {
	var categories = [];
	for(var category of categoriesModel) {
		categories.push(buildCategoryHeader(category));
		categories.push(buildSkillLevelHeaders());
		
		$.merge(categories,buildSkills(category.skills));
	}
	
	return categories;
}

function buildSkills(skillsModel) {
	var skills = [];
	for(var skill of skillsModel) {
		skills.push(buildSkillLevelView(skill));
	}
	return skills;
}

function buildProgrammerName(reportModel){
	return $('<tr><th class="skillheader" colspan="6"><h2>'+reportModel.name+'</h2></th></tr>');
}

function buildCategoryHeader(category){
	return $('<tr><th class="skillheader" colspan="6"><h3>'+category.name+'</h3></th></tr>');
}

function buildSkillLevelHeaders(){
	return $('<tr><th></th><th>Novice</th><th>Competent</th><th>Proficient</th><th>Expert</th><th>Master</th></tr>');
}

var levelOrder = [ 'novice','competent','proficient','expert'];

function buildSkillLevelView(skill) {
	var progress = calculateProgress(skill);
	
	var row = $('<tr></tr>');
	row.append('<th scope="row" class="skillname">'+skill.name+'</th>');

	for(var level of levelOrder ) {
		row.append(buildSkillProgressBar(level,progress[level]));
	}
	
	if(skill.level.toLowerCase() == 'master') {
		row.append('<td class="master">O</td>');
	}
	
	return row;
}

function calculateProgress(skill) {
	var progress = {novice:0,competent:0,proficient:0,expert:0};
	
	for( var level of levelOrder ) {
		if(skill.level.toLowerCase() != level) {
			progress[level] = 100;
		}
		else {
			progress[level] = skill.progress;
			return progress;
		}
	}
	return progress;

}

function buildSkillProgressBar(cellClass, progress) {
	var cell = '<td class="'+cellClass+'">';
	for(var tick = 0; tick < progress/10; tick++) {
		cell += '+';
	}
	return cell+'</td>';
}


