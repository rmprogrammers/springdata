package org.rmpg.mastery.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.rmpg.mastery.application.standalone.SpringBootMainApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ProgrammerAdmin.class, secure = false)
@ContextConfiguration(classes = SpringBootMainApplication.class)
public class ProgrammerAdminTest
{
    @Autowired
    MockMvc mockMvc;

    @Test
    public void addProgrammer()
        throws Exception
    {
        String content = "";
        MvcResult result = mockMvc.perform(post("/programmer").contentType(MediaType.APPLICATION_JSON).content(content))
            .andExpect(status().isOk()).andReturn();

        Assert.assertThat(result.getResponse().getContentAsString(), Matchers.equalTo("123"));
    }
}
