package org.rmpg.mastery.rest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.rmpg.mastery.application.standalone.SpringBootMainApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@WebMvcTest(value = Logbook.class, secure = false)
@ContextConfiguration(classes = SpringBootMainApplication.class)
public class LogbookRestTest
{
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Logbook logbook;

    @Test
    public void test()
        throws Exception
    {
        // date
        // submitter
        // description
        // skills
        // levels
        // examples

        MvcResult result = mockMvc.perform(post("/programmer/{id}/logbook/entry", 123)).andExpect(status().isOk())
            .andReturn();

        Assert.assertThat("123", equalTo(result.getResponse().getContentAsString()));
    }
}
