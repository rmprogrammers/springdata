package org.rmpg.mastery.repository;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.rmpg.mastery.application.standalone.SpringBootMainApplication;
import org.rmpg.mastery.domain.Programmer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = SpringBootMainApplication.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestPropertySource(properties = "debug=true")
public class ProgrammerJPARepositoryTest
{
    @Autowired
    private ProgrammerJPARepo programmerRepo;

    @Test
    public void test()
    {
        Programmer programmer = programmerRepo.findOne("user");
        assertNotNull(programmer);
    }
}
