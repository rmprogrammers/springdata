package org.rmpg.mastery.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.rmpg.mastery.application.standalone.SpringBootMainApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringBootMainApplication.class)
public class JDBCTest
{
    public final static String USERNAME = "bob";
    private static final String SELECT_QUERY = "select username, password, enabled from users where username = '"
        + USERNAME + "'";

    @Autowired
    private DataSource ds;

    @After
    public void after()
        throws SQLException
    {
        try (Connection c = ds.getConnection(); //
            Statement s = c.createStatement())
        {
            s.execute("delete from users where username = '" + USERNAME + "'");
        }
    }

    @Test
    public void test()
        throws SQLException
    {
        try (Connection c = ds.getConnection())
        {
            try (Statement s = c.createStatement())
            {
                s.execute("INSERT INTO users(username, password, enabled)" //
                    + " VALUES ('" + USERNAME + "', 'blowup', true);");

                assertPasswordEquals("blowup");
            }
        }
    }

    private void assertPasswordEquals(String password)
        throws SQLException
    {
        try (Connection c = ds.getConnection(); //
            Statement s = c.createStatement(); //
            ResultSet rs = s.executeQuery(SELECT_QUERY))
        {
            assertTrue("No results", rs.next());
            assertThat(rs.getString(2), equalTo(password));
        }
    }
}
